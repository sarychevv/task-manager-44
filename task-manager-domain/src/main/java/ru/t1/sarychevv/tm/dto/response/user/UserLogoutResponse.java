package ru.t1.sarychevv.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class UserLogoutResponse extends AbstractResultResponse {

    public UserLogoutResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
