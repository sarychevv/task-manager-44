package ru.t1.sarychevv.tm.api.service.dto;

import ru.t1.sarychevv.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {

}
