package ru.t1.sarychevv.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;

public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    private static final Class<SessionDTO> type = null;

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager, type);
    }

}

